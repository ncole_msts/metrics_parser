import gitlab

# Main variables
GROUP_ID = {INSERT_GROUP_ID}
PRIVATE_TOKEN = {INSERT_PAT}  # Create a PAT: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
global TOTAL_SAST_ENABLED
global TOTAL_DAST_ENABLED
global NUM_PROJECTS


class ProjectInfo:
    def __init__(self, name=None):
        self.project_name = name
        self.dast_enabled = False
        self.sast_enabled = False


def print_info(project_info_dict):
    dast_projects = project_info_dict.get('dast')
    sast_projects = project_info_dict.get('sast')
    other_projects = project_info_dict.get('other')

    print("\nPROJECT LIST")
    print("========================================")
    print("\nProjects with DAST enabled:")
    for project in dast_projects:
        print(project.project_name)

    print("\nProjects with SAST enabled:")
    for project in sast_projects:
        print(project.project_name)

    print("\nProjects with neither SAST nor DAST")
    for project in other_projects:
        print(project.project_name)

    print("\nTOTALS")
    print("========================================")
    print("Total with DAST: " + str(len(dast_projects)))
    print("Total with SAST: " + str(len(sast_projects)))
    print("Total projects: " + str(len(dast_projects) + len(sast_projects) + len(other_projects)))


def get_metrics(gl, projects_list):
    print("Getting metrics...")

    other_projects = []
    projects_with_dast = []
    projects_with_sast = []

    for project in projects_list:
        # Get individual project data
        project = gl.projects.get(project.id)

        print("...")

        project_info = ProjectInfo(name=project.name)

        try:
            # Get and decode .gitlab-ci.yml file contents
            f = project.files.get(file_path='.gitlab-ci.yml', ref='master')
            yml_contents = str(f.decode())

            if "DAST.gitlab-ci.yml" in yml_contents:
                project_info.dast_enabled = True
                projects_with_dast.append(project_info)
            if "SAST.gitlab-ci.yml" in yml_contents:
                project_info.sast_enabled = True
                projects_with_sast.append(project_info)

        except(Exception):
            other_projects.append(project_info)
            pass

    # Return dict with projects by feature enabled
    return {'other': other_projects, 'dast': projects_with_dast, 'sast': projects_with_sast}


def get_projects(gl):
    group = gl.groups.get(GROUP_ID)
    return group.projects.list(all=True)


def authenticate():
    gl = gitlab.Gitlab('https://gitlab.com', private_token=PRIVATE_TOKEN)
    print("Begin authenticating...")
    gl.auth()
    print("Authentication successful!")
    return gl


def main():
    gl = authenticate()
    projects_list = get_projects(gl)
    project_info_dict = get_metrics(gl, projects_list)
    print_info(project_info_dict)

main()
